defmodule Changer do
  @moduledoc """
  Provides tools for composing transformations on maps. Transformations accumulate an
  audit trail.
  """
end
