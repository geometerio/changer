defmodule Changer.Transformer do
  @moduledoc """
  Transformers provide a means of repeatably applying a
  series of mapping functions from one data structure to another, with
  auditable visibility into every transformation
  """

  alias Changer.Transformer.AuditEntry
  alias Changer.Transformer.Error

  @type audit_trail() :: list(AuditEntry.t())

  @type response() :: {:ok, map(), audit_trail()} | {:error, Error.t()}
  @type atom_or_binary() :: atom() | binary()

  @type input() :: map() | input_with_audits() | ok_input_with_audits() | {:error, Error.t()}
  @type input_with_audits() :: {map(), audit_trail()}
  @type ok_input_with_audits() :: {:ok, map(), audit_trail()}

  @type predicate() :: {atom_or_binary() | list(atom_or_binary()), atom(), any()}

  @doc """
  Terminates a transformer pipeline, stripping off the audit trail.

  If there is a configured audit logger, in the future this will save the audit trail
  to that logger.
  """
  @spec value(response()) :: {:ok, map()} | {:error, Error.t()}
  def value({:ok, value, _audit_trail}), do: {:ok, value}
  def value({:error, reason}), do: {:error, reason}

  @doc """
  Terminates a transformer pipeline, raising a `Changer.Transformer.Error` if
  the pipeline was unsuccessful.
  """
  @spec value!(response()) :: map()
  def value!({:ok, value, _audit_trail}), do: value
  def value!({:error, reason}), do: raise(reason)

  defmacro __using__(_) do
    quote do
      import Changer.Transformer.Macro, only: :macros
    end
  end
end
