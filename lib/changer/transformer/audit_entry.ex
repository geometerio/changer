defmodule Changer.Transformer.AuditEntry do
  @moduledoc """
  Audit Entry represents a single transformation applied to an input map. It
  records the name and arguments provided to the transformer, as well
  as the input and output data
  """

  @type t() :: %__MODULE__{}

  #  @enforce_keys ~w[ name args input output ]a
  defstruct [
    :name,
    :args,
    :input,
    :output,
    sub_entries: []
  ]
end
