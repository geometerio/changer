defmodule Changer.Transformer.Transformers do
  @moduledoc """
  Default transformer vocabulary for Changer
  """

  use Changer.Transformer
  use Magritte

  alias Changer.Transformer, as: T

  @doc """
  Copies a value between keys.

  ## Examples

      iex> %{a: 1}
      ...> |> Changer.Transformer.Transformers.copy_key(:a, :b)
      ...> |> Changer.Transformer.value()
      {:ok, %{a: 1, b: 1}}
  """
  @spec copy_key(T.input(), from_key :: T.atom_or_binary(), to_key :: T.atom_or_binary()) ::
          T.response()
  deftransformer copy_key(input, from_key, to_key) do
    cond do
      Map.has_key?(input, to_key) ->
        {:error, "map already has destination key"}

      Map.has_key?(input, from_key) ->
        input
        |> Map.get(from_key)
        |> Map.put(input, to_key, ...)
        |> Tuple.append({:ok}, ...)

      true ->
        {:ok, input}
    end
  end

  @doc """
  Given a key with a value that is a list of maps, filters the list based on a predicate.

  ## Examples

      iex> %{input: [%{a: 1}, %{a: 2}, %{c: 3}]}
      ...> |> Changer.Transformer.Transformers.filter_list(:input, {:a, :>, 1})
      ...> |> Changer.Transformer.value()
      {:ok, %{input: [%{a: 2}]}}
  """
  @spec filter_list(
          T.input(),
          parent_key :: T.atom_or_binary(),
          predicate :: T.predicate()
        ) ::
          T.response()
  deftransformer filter_list(input, parent_key, {key, predicate, value}) do
    reducer = fn
      _, {:error, reason} ->
        {:error, reason}

      %{} = map, {:ok, acc} ->
        key
        |> List.wrap()
        |> get_in(map, ...)
        |> case do
          nil ->
            {:ok, acc}

          v ->
            if Kernel.apply(Kernel, predicate, [v, value]),
              do: {:ok, [map | acc]},
              else: {:ok, acc}
        end

      _, {:ok, _acc} ->
        {:error, "expected to be a list of maps"}
    end

    with {:ok, list} <- get_list(input, parent_key),
         {:ok, filtered_list} <- Enum.reduce(list, {:ok, []}, reducer) do
      filtered_list
      |> Enum.reverse()
      |> Map.put(input, parent_key, ...)
      |> Tuple.append({:ok}, ...)
    end
  end

  @doc """
  Given a key with a map value, flattens the sub-map onto the parent.

  ## Examples

      iex> %{input: %{a: 1}, b: 2}
      ...> |> Changer.Transformer.Transformers.flatten_nested(:input)
      ...> |> Changer.Transformer.value()
      {:ok, %{a: 1, b: 2}}
  """
  @spec flatten_nested(T.input(), key :: T.atom_or_binary()) :: T.response()
  deftransformer flatten_nested(input, key) do
    {nested, outer} = input |> Map.pop(key)

    case nested do
      nil ->
        {:ok, outer}

      %{} ->
        nested_keys = nested |> Map.keys() |> MapSet.new()

        if outer |> Map.keys() |> MapSet.new() |> MapSet.disjoint?(nested_keys) do
          {:ok, Map.merge(outer, nested)}
        else
          {:error, "key already exists in outer map"}
        end

      _ ->
        {:error, "value is not a map"}
    end
  end

  @doc """
  Renames a key.

  ## Examples

      iex> %{input: 1}
      ...> |> Changer.Transformer.Transformers.rename_key(:input, :output)
      ...> |> Changer.Transformer.value()
      {:ok, %{output: 1}}
  """
  @spec rename_key(T.input(), from :: T.atom_or_binary(), to :: T.atom_or_binary()) :: T.response()
  deftransformer rename_key(input_data, from_key_name, to_key_name) do
    cond do
      Map.has_key?(input_data, to_key_name) ->
        {:error, "map already has destination key"}

      Map.has_key?(input_data, from_key_name) ->
        value = input_data |> Map.get(from_key_name)

        input_data
        |> Map.delete(from_key_name)
        |> Map.put(to_key_name, value)
        |> Tuple.append({:ok}, ...)

      true ->
        {:ok, input_data}
    end
  end

  @doc """
  Receives a map of canonical values, and updates the value of key to match.

  ## Examples

      iex> %{input: "Yep"}
      ...> |> Changer.Transformer.Transformers.canonicalize(:input, %{"Yep" => true, "Nope" => false}, default: false)
      ...> |> Changer.Transformer.value()
      {:ok, %{input: true}}
  """
  @spec canonicalize(T.input(), key :: T.atom_or_binary(), lookup :: map(), [{:default, any()}]) ::
          T.response()
  deftransformer canonicalize(input_data, key, lookup_map, default: default) do
    if Map.has_key?(input_data, key) do
      input_data
      |> Map.get(key)
      |> Map.get(lookup_map, ...)
      |> Map.put(input_data, key, ...)
      |> Tuple.append({:ok}, ...)
    else
      {:ok, input_data}
    end
  end

  @doc """
  Applies a function to every member of the list found at `key` within `input`. This expects
  each member of the list to be a map. The function must return a `t:Changer.Transformer.response/0`.

  ## Errors

    * Value of `key` is not a list
    * Any member of the list is not a map
    * Any sub-transformer returns an `{:error, reason}`

  ## Examples

      iex> alias Changer.Transformer.Transformers
      iex> %{outer: [%{inner: "1"}, %{inner: "2"}]}
      ...> |> Transformers.map(:outer, fn child ->
      ...>       child
      ...>       |> Transformers.parse_int(:inner)
      ...>       |> Transformers.rename_key(:inner, :nested)
      ...> end)
      ...> |> Changer.Transformer.value()
      {:ok, %{outer: [%{nested: 1}, %{nested: 2}]}}
  """
  @spec map(T.input(), key :: T.atom_or_binary(), fun :: (map() -> T.response())) :: T.response()
  deftransformer map(input, key, fun) do
    reducer = fn
      _child, {:error, reason} ->
        {:error, reason}

      child, {:ok, children, logs} when is_map(child) ->
        with {:ok, transformed_child, audit_log} <- fun.(child) do
          {:ok, [transformed_child | children], [audit_log | logs]}
        end

      _child, {:ok, _children, _logs} ->
        {:error, "list member was not a map"}
    end

    with {:ok, list} <- get_list(input, key),
         {:ok, children, audit_logs} <- list |> Enum.reduce({:ok, [], []}, reducer) do
      {:ok, Map.put(input, key, Enum.reverse(children)), audit_logs}
    end
  end

  defp get_list(input, key) do
    if Map.has_key?(input, key) do
      input
      |> Map.get(key)
      |> case do
        list when is_list(list) -> {:ok, list}
        _ -> {:error, "value was not a list"}
      end
    else
      {:error, "key not present in input"}
    end
  end

  @doc """
  Given a key with a string value, parse it as a `Float`.

  ## Examples

      iex> %{a: "10.5"}
      ...> |> Changer.Transformer.Transformers.parse_float(:a)
      ...> |> Changer.Transformer.value()
      {:ok, %{a: 10.5}}
  """
  @spec parse_float(T.input(), key :: T.atom_or_binary()) :: T.response()
  deftransformer parse_float(input, key) do
    if Map.has_key?(input, key) do
      input
      |> Map.get(key)
      |> case do
        string when is_binary(string) ->
          string
          |> Float.parse()
          |> case do
            :error ->
              {:error, "could not parse value to a float"}

            {n, _} ->
              {:ok, Map.put(input, key, n)}
          end

        _ ->
          {:error, "value was not a string"}
      end
    else
      {:ok, input}
    end
  end

  @doc """
  Given a key with a string value, parse it as a `NaiveDateTime` using `Timex`.

  ## Examples

      iex> %{a: "2000-01-01 12:00:00Z"}
      ...> |> Changer.Transformer.Transformers.parse_datetime(:a, "{ISO:Extended}")
      ...> |> Changer.Transformer.value()
      {:ok, %{a: ~U[2000-01-01 12:00:00Z]}}

      iex> %{a: "2000-01-01 12:00:00"}
      ...> |> Changer.Transformer.Transformers.parse_datetime(:a, "{ISO:Extended}")
      ...> |> Changer.Transformer.value()
      {:ok, %{a: ~N[2000-01-01 12:00:00]}}
  """
  @spec parse_datetime(T.input(), key :: T.atom_or_binary(), format :: binary()) :: T.response()
  deftransformer parse_datetime(input, key, format_string) do
    if Map.has_key?(input, key) do
      input
      |> Map.get(key)
      |> case do
        string when is_binary(string) ->
          string
          |> Timex.parse(format_string)
          |> case do
            {:ok, result} ->
              {:ok, Map.put(input, key, result)}

            {:error, _reason} ->
              {:error, "unable to parse datetime"}
          end

        _ ->
          {:error, "value was not a string"}
      end
    else
      {:ok, input}
    end
  end

  @doc """
  Given a key with a string value, parse it as an `Integer`.

  ## Examples

      iex> %{a: "10"}
      ...> |> Changer.Transformer.Transformers.parse_int(:a)
      ...> |> Changer.Transformer.value()
      {:ok, %{a: 10}}
  """
  deftransformer parse_int(input, key) do
    if Map.has_key?(input, key) do
      input
      |> Map.get(key)
      |> case do
        string when is_binary(string) ->
          string
          |> Integer.parse()
          |> case do
            {n, ""} ->
              {:ok, Map.put(input, key, n)}

            _ ->
              {:error, "could not parse value to a int"}
          end

        _ ->
          {:error, "value was not a string"}
      end
    else
      {:ok, input}
    end
  end

  @doc """
  Parses a whitespace-delimited string as a `MapSet`.

  ## Examples

      iex> %{a: "1 2 a    b c a"}
      ...> |> Changer.Transformer.Transformers.parse_set(:a)
      ...> |> Changer.Transformer.value()
      {:ok, %{a: MapSet.new(["1", "2", "a", "b", "c"])}}
  """
  @spec parse_set(T.input(), key :: T.atom_or_binary()) :: T.response()
  deftransformer parse_set(input, key) do
    if Map.has_key?(input, key) do
      input
      |> Map.get(key)
      |> case do
        s when is_binary(s) ->
          s
          |> String.split()
          |> MapSet.new()
          |> Map.put(input, key, ...)
          |> Tuple.append({:ok}, ...)

        _ ->
          {:error, "value was not a string"}
      end
    else
      {:ok, input}
    end
  end

  @doc """
  Camelizes the keys of the input map.

  ## Examples

      iex> %{a: 1, some_thing: 2}
      ...> |> Changer.Transformer.Transformers.camelize_keys()
      ...> |> Changer.Transformer.value()
      {:ok, %{"a" => 1, "someThing" => 2}}
  """
  @spec camelize_keys(T.input()) :: T.response()
  deftransformer camelize_keys(input) do
    input
    |> Enum.map(fn {k, v} -> {k |> Kernel.to_string() |> Inflex.camelize(:lower), v} end)
    |> Map.new()
    |> Tuple.append({:ok}, ...)
  end

  @doc """
  Underscores the keys of the input map.

  ## Examples

      iex> %{:a => 1, "SomeThing" => 2}
      ...> |> Changer.Transformer.Transformers.underscore_keys()
      ...> |> Changer.Transformer.value()
      {:ok, %{"a" => 1, "some_thing" => 2}}
  """
  @spec underscore_keys(T.input()) :: T.response()
  deftransformer underscore_keys(input) do
    input
    |> Enum.map(fn {k, v} -> {k |> Kernel.to_string() |> Inflex.underscore(), v} end)
    |> Map.new()
    |> Tuple.append({:ok}, ...)
  end

  @doc """
  Removes all keys with a value of nil.

  ## Examples

      iex> %{a: 1, b: nil, c: 2}
      ...> |> Changer.Transformer.Transformers.drop_nil_values()
      ...> |> Changer.Transformer.value()
      {:ok, %{a: 1, c: 2}}
  """
  @spec drop_nil_values(T.input()) :: T.response()
  deftransformer drop_nil_values(input) do
    input
    |> Enum.filter(fn {_k, v} -> v != nil end)
    |> Map.new()
    |> Tuple.append({:ok}, ...)
  end

  @doc """
  Replaces a sub-map with its values.

  ## Examples

      iex> %{outer: %{inner: 1, other: :thing}, other: %{}}
      ...> |> Changer.Transformer.Transformers.values_as_list(:outer)
      ...> |> Changer.Transformer.value()
      {:ok, %{outer: [1, :thing], other: %{}}}
  """
  @spec values_as_list(T.input(), key :: T.atom_or_binary()) :: T.response()
  deftransformer values_as_list(input, key) do
    input
    |> Map.get(key)
    |> case do
      %{} = map ->
        map
        |> Map.values()
        |> Map.put(input, key, ...)
        |> Tuple.append({:ok}, ...)

      _ ->
        {:error, "value is not a map"}
    end
  end
end
