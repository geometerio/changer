# credo:disable-for-this-file
defmodule Changer.Transformer.Macro do
  @moduledoc """
  Provides the deftransformer/2 macro
  """

  alias Changer.Transformer.AuditEntry
  alias Changer.Transformer.Error

  @doc """
  Generate the function heads to accumulate transformations maps, with an audit trail.

  Generated transformer functions will return: `{:ok, new_map, audit_trail}` or `{:error, reason}`.

  Transformers arity can be whatever is desired. Transformer definitions should return one of:
  `{:ok, new_map}`, `{:error, %Changer.Transformer.Error{}}`, or the return value of a transformer.

  ## Usage

      use Changer.Transformer

      deftransformer do_stuff(input, key) do
        updated_input = input |> Map.put(key, "value")
        {:ok, updated_input}
      end

  ## Generated functions

  The above `deftransformer` will generate the following function heads:

      @spec  do_stuff(input :: map(), key :: any()) :: {:ok, map(), audit_trail()} | {:error, error}
      @spec  do_stuff({input :: map(), audit_trail()}, key :: any()) :: {:ok, map(), audit_trail()} | {:error, error}
      @spec  do_stuff({:ok, input :: map(), audit_trail()}, key :: any()) :: {:ok, map(), audit_trail()} | {:error, error}

      @spec do_stuff!(input :: map(), key :: any()) :: {map(), audit_trail()}
      @spec do_stuff!({input :: map(), audit_trail()}, key :: any()) :: {map(), audit_trail()}
      @spec do_stuff!({:ok, input :: map(), audit_trail()}, key :: any()) :: {map(), audit_trail()}

  Bang functions will raise a `Changer.Transformer.Error` if the transformer definition returns
  `{:error, reason}`.
  """
  defmacro deftransformer(signature, do: body) do
    {name, [input | args]} = Macro.decompose_call(signature)
    arity = args |> length() |> Kernel.+(1)
    audit = Macro.var(:audit, __MODULE__)
    error_arg = quote do: unquote(input) = {:error, error}
    input_arg = quote do: unquote(input) = %{}

    input_arg_with_ok_and_audit =
      quote do
        {:ok, unquote(input_arg), unquote(audit)}
      end

    input_arg_with_audit = {input_arg, audit}

    quote generated: true do
      def unquote(name)(unquote_splicing([input_arg_with_ok_and_audit | args])) do
        output = unquote(body)

        output
        |> case do
          {:error, reason} ->
            {:error,
             %Error{
               operation: unquote(name),
               args: unquote(args),
               input: unquote(input),
               reason: reason
             }}

          {:ok, output} ->
            {:ok, output,
             [
               %AuditEntry{
                 name: unquote(name),
                 args: unquote(args),
                 input: unquote(input),
                 output: output
               }
               | audit
             ]}

          {:ok, output, sub_entries} when is_list(sub_entries) ->
            {:ok, output,
             [
               %AuditEntry{
                 name: unquote(name),
                 args: unquote(args),
                 input: unquote(input),
                 output: output,
                 sub_entries: sub_entries
               }
               | audit
             ]}
        end
      end

      def unquote(name)(unquote_splicing([input_arg_with_audit | args])) do
        unquote(name)(
          unquote_splicing([
            quote do
              {:ok, unquote(input), unquote(audit)}
            end
            | args
          ])
        )
      end

      def unquote(name)(unquote_splicing([input_arg | args])) do
        unquote(name)(
          unquote_splicing([
            quote do
              {:ok, unquote(input), []}
            end
            | args
          ])
        )
      end

      def unquote(name)(unquote_splicing([error_arg | args])) do
        unquote(args)
        unquote(input)
      end

      @doc """
      See `#{unquote(name)}/#{unquote(arity)}`. Raises on `{:error, error}`.
      """
      def unquote(bang_ast(name))(unquote_splicing([input_arg_with_ok_and_audit | args])) do
        unquote(name)(
          unquote_splicing([
            quote do
              {:ok, unquote(input), unquote(audit)}
            end
            | args
          ])
        )
        |> case do
          {:error, %Error{} = error} -> raise error
          {:ok, output, audit} -> {output, audit}
        end
      end

      def unquote(bang_ast(name))(unquote_splicing([input_arg_with_audit | args])) do
        unquote(bang_ast(name))(
          unquote_splicing([
            quote do
              {:ok, unquote(input), unquote(audit)}
            end
            | args
          ])
        )
      end

      def unquote(bang_ast(name))(unquote_splicing([input_arg | args])) do
        unquote(bang_ast(name))(
          unquote_splicing([
            quote do
              {:ok, unquote(input), []}
            end
            | args
          ])
        )
      end

      def unquote(bang_ast(name))(unquote_splicing([error_arg | args])) do
        unquote(args)

        {:error, error} = unquote(input)
        raise(error)
      end
    end
  end

  defp bang_ast(name),
    do: name |> Atom.to_string() |> Kernel.<>("!") |> String.to_atom()
end
