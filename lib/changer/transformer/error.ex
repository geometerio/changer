defmodule Changer.Transformer.Error do
  @type t :: %__MODULE__{}
  defexception [:operation, :args, :input, :reason]

  def message(%__MODULE__{reason: reason}) when is_binary(reason) do
    reason
  end

  def message(%__MODULE__{reason: %__MODULE__{} = suberror}) do
    message(suberror)
  end

  def message(%__MODULE__{}), do: "Changer.Transformer error with unknown reason"
end
