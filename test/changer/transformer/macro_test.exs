defmodule Changer.Transformer.MacroTest do
  use ExUnit.Case
  alias Changer.Transformer.AuditEntry
  alias Changer.Transformer.Error

  defmodule TestTransformers do
    use Changer.Transformer

    deftransformer test_transformation(input, arg1, arg2) do
      case input do
        %{error: message} -> {:error, message}
        _ -> {:ok, input |> Map.put(arg1, arg2)}
      end
    end

    deftransformer test_nested_transformation(input, arg) do
      case input do
        %{error: message} ->
          {:error, message}

        _ ->
          input
          |> test_transformation(:a, "#{arg}_a")
          |> test_transformation(:b, "#{arg}_b")
      end
    end
  end

  describe "not bang ok" do
    @input %{ok: :ok}
    @expected_audit_entry %AuditEntry{
      args: [:first_arg, :second_arg],
      name: :test_transformation,
      input: %{ok: :ok},
      output: %{ok: :ok, first_arg: :second_arg}
    }

    @expected_output %{ok: :ok, first_arg: :second_arg}
    @preexisting_audit_entry :preexisting_audit_entry
    test "with just a map" do
      return_value = TestTransformers.test_transformation(@input, :first_arg, :second_arg)
      assert {:ok, @expected_output, [@expected_audit_entry]} == return_value
    end

    test "with a map and audit" do
      return_value =
        TestTransformers.test_transformation(
          {@input, [@preexisting_audit_entry]},
          :first_arg,
          :second_arg
        )

      assert {:ok, @expected_output, [@expected_audit_entry, @preexisting_audit_entry]} ==
               return_value
    end

    test "with an {:ok, map, audit} tuple" do
      return_value =
        TestTransformers.test_transformation(
          {:ok, @input, [@preexisting_audit_entry]},
          :first_arg,
          :second_arg
        )

      assert {:ok, @expected_output, [@expected_audit_entry, @preexisting_audit_entry]} ==
               return_value
    end
  end

  describe "not bang error" do
    @input %{error: "error message"}
    @expected_output {
      :error,
      %Error{
        args: [:first_arg, :second_arg],
        input: %{error: "error message"},
        reason: "error message",
        operation: :test_transformation
      }
    }

    test "with just a map" do
      return_value = TestTransformers.test_transformation(@input, :first_arg, :second_arg)
      assert @expected_output == return_value
    end

    test "with a map and audit" do
      return_value =
        TestTransformers.test_transformation(
          {@input, [@preexisting_audit_entry]},
          :first_arg,
          :second_arg
        )

      assert @expected_output == return_value
    end

    test "with an {:ok, map, audit} tuple" do
      return_value =
        TestTransformers.test_transformation(
          {:ok, @input, [@preexisting_audit_entry]},
          :first_arg,
          :second_arg
        )

      assert @expected_output == return_value
    end
  end

  describe "not bang with {:error, Error}" do
    test "returns the error" do
      error = {:error, %Error{}}

      assert TestTransformers.test_transformation(
               error,
               :first_arg,
               :second_arg
             ) == error
    end
  end

  describe "bang success" do
    @input %{ok: :ok}
    @expected_audit_entry %AuditEntry{
      args: [:first_arg, :second_arg],
      name: :test_transformation,
      input: %{ok: :ok},
      output: %{ok: :ok, first_arg: :second_arg}
    }

    @expected_output %{ok: :ok, first_arg: :second_arg}
    @preexisting_audit_entry :preexisting_audit_entry
    test "with just a map" do
      return_value = TestTransformers.test_transformation!(@input, :first_arg, :second_arg)
      assert {@expected_output, [@expected_audit_entry]} == return_value
    end

    test "with a map and audit" do
      return_value =
        TestTransformers.test_transformation!(
          {@input, [@preexisting_audit_entry]},
          :first_arg,
          :second_arg
        )

      assert {@expected_output, [@expected_audit_entry, @preexisting_audit_entry]} ==
               return_value
    end

    test "with an {:ok, map, audit} tuple" do
      return_value =
        TestTransformers.test_transformation!(
          {:ok, @input, [@preexisting_audit_entry]},
          :first_arg,
          :second_arg
        )

      assert {@expected_output, [@expected_audit_entry, @preexisting_audit_entry]} ==
               return_value
    end
  end

  describe "bang failure" do
    @input %{error: "error message"}
    @expected_error %Error{
      args: [:first_arg, :second_arg],
      input: %{error: "error message"},
      reason: "error message",
      operation: :test_transformation
    }

    test "with just a map" do
      error =
        assert_raise Error, fn ->
          TestTransformers.test_transformation!(@input, :first_arg, :second_arg)
        end

      assert @expected_error == error
    end

    test "with a map and audit" do
      error =
        assert_raise Error, fn ->
          TestTransformers.test_transformation!(
            {@input, [@preexisting_audit_entry]},
            :first_arg,
            :second_arg
          )
        end

      assert @expected_error == error
    end

    test "with an {:ok, map, audit} tuple" do
      error =
        assert_raise Error, fn ->
          TestTransformers.test_transformation!(
            {:ok, @input, [@preexisting_audit_entry]},
            :first_arg,
            :second_arg
          )
        end

      assert @expected_error == error
    end

    test "with an {:error, Error}" do
      assert_raise Error, fn ->
        TestTransformers.test_transformation!(
          {:error, %Error{}},
          :first_arg,
          :second_arg
        )
      end
    end
  end

  describe "nested transformer that returns an audit log" do
    test "with a map" do
      {:ok, output, [audit]} = TestTransformers.test_nested_transformation(%{}, :arg)
      assert output == %{a: "arg_a", b: "arg_b"}

      assert audit == %AuditEntry{
               args: [:arg],
               name: :test_nested_transformation,
               input: %{},
               output: %{a: "arg_a", b: "arg_b"},
               sub_entries: [
                 %AuditEntry{
                   args: [:b, "arg_b"],
                   name: :test_transformation,
                   input: %{a: "arg_a"},
                   output: %{a: "arg_a", b: "arg_b"}
                 },
                 %AuditEntry{
                   args: [:a, "arg_a"],
                   name: :test_transformation,
                   input: %{},
                   output: %{a: "arg_a"}
                 }
               ]
             }
    end
  end
end
