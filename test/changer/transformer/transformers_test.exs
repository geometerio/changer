defmodule Changer.Transformer.TransformersTest do
  use ExUnit.Case
  alias Changer.Transformer.AuditEntry
  alias Changer.Transformer.Error
  alias Changer.Transformer.Transformers
  doctest Changer.Transformer.Transformers

  describe "copy_key" do
    test "when the source key exists and there isn't a preexisting destination key" do
      input_data = %{input: "some data", unused: :value}
      expected = %{input: "some data", output: "some data", unused: :value}
      {:ok, output, audit_trail} = Transformers.copy_key(input_data, :input, :output)

      assert audit_trail == [
               %AuditEntry{
                 name: :copy_key,
                 args: [:input, :output],
                 input: input_data,
                 output: expected
               }
             ]

      assert output == expected
    end

    test "does nothing when the source key doesn't exist" do
      test_data = %{unused: :unused}
      expected = test_data
      {:ok, output, audit_trail} = test_data |> Transformers.copy_key(:input, :output)

      assert audit_trail == [
               %AuditEntry{
                 name: :copy_key,
                 args: [:input, :output],
                 input: test_data,
                 output: expected
               }
             ]

      assert output == expected
    end

    test "when from_key exists and value is nil" do
      test_data = %{input: nil, unused: :value}
      expected = %{input: nil, output: nil, unused: :value}
      {:ok, output, audit_trail} = Transformers.copy_key(test_data, :input, :output)

      assert audit_trail == [
               %AuditEntry{
                 name: :copy_key,
                 args: [:input, :output],
                 input: test_data,
                 output: expected
               }
             ]

      assert output == expected
    end

    test "when the destination key exists" do
      test_data = %{input: "a", unused: :unused, output: :uh_oh}

      {:error, error} = test_data |> Transformers.copy_key(:input, :output)

      assert error == %Error{
               args: [:input, :output],
               input: %{input: "a", output: :uh_oh, unused: :unused},
               reason: "map already has destination key",
               operation: :copy_key
             }
    end
  end

  describe "filter_list" do
    test "when value of parent key is a list of maps" do
      input_data = %{input: [%{thing: 1, name: "a"}, %{thing: 2, name: "b"}], unused: :value}
      expected = %{input: [%{thing: 1, name: "a"}], unused: :value}
      {:ok, output, audit_trail} = Transformers.filter_list(input_data, :input, {:thing, :==, 1})

      assert audit_trail == [
               %AuditEntry{
                 args: [:input, {:thing, :==, 1}],
                 input: input_data,
                 name: :filter_list,
                 output: expected
               }
             ]

      assert output == expected
    end

    test "when predicate is a list of keys" do
      input_data = %{input: [%{thing: %{value: 1}}, %{thing: %{value: 2}}], unused: :value}
      expected = %{input: [%{thing: %{value: 1}}], unused: :value}
      {:ok, output, audit_trail} = Transformers.filter_list(input_data, :input, {[:thing, :value], :==, 1})

      assert audit_trail == [
               %AuditEntry{
                 args: [:input, {[:thing, :value], :==, 1}],
                 input: input_data,
                 name: :filter_list,
                 output: expected
               }
             ]

      assert output == expected
    end

    test "when value of parent key is a list of maps, with :> predicate" do
      input_data = %{input: [%{thing: 1, name: "a"}, %{thing: 2, name: "b"}], unused: :value}
      expected = %{input: [%{thing: 2, name: "b"}], unused: :value}
      {:ok, output, audit_trail} = Transformers.filter_list(input_data, :input, {:thing, :>, 1})

      assert audit_trail == [
               %AuditEntry{
                 args: [:input, {:thing, :>, 1}],
                 input: input_data,
                 name: :filter_list,
                 output: expected
               }
             ]

      assert output == expected
    end

    test "when value of parent key is a list of maps, but nested map does not contain key" do
      input_data = %{input: [%{thing: 1, name: "a"}, %{name: "b"}], unused: :value}
      expected = %{input: [%{thing: 1, name: "a"}], unused: :value}
      {:ok, output, audit_trail} = Transformers.filter_list(input_data, :input, {:thing, :>=, 1})

      assert audit_trail == [
               %AuditEntry{
                 args: [:input, {:thing, :>=, 1}],
                 input: input_data,
                 name: :filter_list,
                 output: expected
               }
             ]

      assert output == expected
    end

    test "when parent key does not exist" do
      input_data = %{unused: :value}
      {:error, error} = Transformers.filter_list(input_data, :input, {:thing, :==, 1})

      assert error == %Error{
               args: [:input, {:thing, :==, 1}],
               input: input_data,
               reason: "key not present in input",
               operation: :filter_list
             }
    end

    test "when value of parent key exists and is not a list" do
      input_data = %{input: 12, unused: :value}
      {:error, error} = Transformers.filter_list(input_data, :input, {:thing, :==, 1})

      assert error == %Error{
               args: [:input, {:thing, :==, 1}],
               input: input_data,
               reason: "value was not a list",
               operation: :filter_list
             }
    end

    test "when value of parent key exists is not a list of maps" do
      input_data = %{input: [1, 2], unused: :value}
      {:error, error} = Transformers.filter_list(input_data, :input, {:thing, :==, 1})

      assert error == %Error{
               args: [:input, {:thing, :==, 1}],
               input: input_data,
               reason: "expected to be a list of maps",
               operation: :filter_list
             }
    end
  end

  describe "flatten_nested" do
    test "when key exists and has a map value" do
      input_data = %{input: %{some: "thing", other: "stuff"}, unused: :value}
      expected = %{some: "thing", other: "stuff", unused: :value}
      {:ok, output, audit_trail} = Transformers.flatten_nested(input_data, :input)

      assert audit_trail == [
               %AuditEntry{
                 name: :flatten_nested,
                 args: [:input],
                 input: input_data,
                 output: expected
               }
             ]

      assert output == expected
    end

    test "when key exists and has a non-map value" do
      input_data = %{input: ["thing"], unused: :value}
      {:error, error} = Transformers.flatten_nested(input_data, :input)

      assert error == %Error{
               args: [:input],
               input: input_data,
               reason: "value is not a map",
               operation: :flatten_nested
             }
    end

    test "when key does not exist" do
      input_data = %{unused: :value}
      expected = %{unused: :value}
      {:ok, output, audit_trail} = Transformers.flatten_nested(input_data, :input)

      assert audit_trail == [
               %AuditEntry{
                 name: :flatten_nested,
                 args: [:input],
                 input: input_data,
                 output: expected
               }
             ]

      assert output == expected
    end

    test "when outer map has keys present in inner map" do
      input_data = %{input: %{overwriting: "thing"}, overwriting: "stuff", unused: :value}
      {:error, error} = Transformers.flatten_nested(input_data, :input)

      assert error == %Error{
               args: [:input],
               input: input_data,
               reason: "key already exists in outer map",
               operation: :flatten_nested
             }
    end
  end

  describe "rename_key" do
    test "when the key exists and there isn't a preexisting value" do
      input_data = %{input: "some data", unused: :value}
      expected = %{output: "some data", unused: :value}
      {:ok, output, audit_trail} = Transformers.rename_key(input_data, :input, :output)

      assert audit_trail == [
               %AuditEntry{
                 name: :rename_key,
                 args: [:input, :output],
                 input: input_data,
                 output: expected
               }
             ]

      assert output == expected
    end

    test "when the key doesn't exist" do
      test_data = %{unused: :unused}
      expected = test_data
      {:ok, output, audit_trail} = test_data |> Transformers.rename_key(:input, :output)

      assert audit_trail == [
               %AuditEntry{
                 name: :rename_key,
                 args: [:input, :output],
                 input: test_data,
                 output: expected
               }
             ]

      assert output == expected
    end

    test "when from_key exists and value is nil" do
      test_data = %{input: nil, unused: :value}
      expected = %{output: nil, unused: :value}
      {:ok, output, audit_trail} = Transformers.rename_key(test_data, :input, :output)

      assert audit_trail == [
               %AuditEntry{
                 name: :rename_key,
                 args: [:input, :output],
                 input: test_data,
                 output: expected
               }
             ]

      assert output == expected
    end

    test "when the destination key exists" do
      test_data = %{unused: :unused, output: :uh_oh}

      {:error, error} = test_data |> Transformers.rename_key(:input, :output)

      assert error == %Error{
               args: [:input, :output],
               input: %{output: :uh_oh, unused: :unused},
               reason: "map already has destination key",
               operation: :rename_key
             }
    end
  end

  describe "canonicalize" do
    @truthiness %{"no" => false, "yes" => true}

    test "when the value exists in the lookup map" do
      test_data = %{some_key: "yes"}
      expected = %{some_key: true}

      {:ok, output, audit} = Transformers.canonicalize(test_data, :some_key, @truthiness, default: nil)

      assert output == expected

      assert audit == [
               %AuditEntry{
                 name: :canonicalize,
                 args: [:some_key, @truthiness, [default: nil]],
                 input: test_data,
                 output: expected
               }
             ]
    end

    test "when the value does not exist in the lookup map" do
      test_data = %{some_key: "maybe"}
      expected = %{some_key: nil}

      {:ok, output, audit} = Transformers.canonicalize(test_data, :some_key, @truthiness, default: nil)

      assert output == expected

      assert audit == [
               %AuditEntry{
                 name: :canonicalize,
                 args: [:some_key, @truthiness, [default: nil]],
                 input: test_data,
                 output: expected
               }
             ]
    end

    test "when key does not exist in the input data" do
      test_data = %{unused: :unused}
      expected = test_data
      key = :does_not_exist

      {:ok, output, audit} = Transformers.canonicalize(test_data, key, @truthiness, default: 10)

      assert output == expected

      assert audit == [
               %AuditEntry{
                 name: :canonicalize,
                 args: [key, @truthiness, [default: 10]],
                 input: test_data,
                 output: expected
               }
             ]
    end
  end

  test "combining a bunch of transformations" do
    {:ok, output, audit} =
      %{input_key: "no", unused: :unused, number: "10", created: "2016-02-29", blank: ""}
      |> Transformers.rename_key(:input_key, :output_key)
      |> Transformers.canonicalize(:output_key, %{"no" => false}, default: nil)
      |> Transformers.canonicalize(:blank, %{"" => nil}, default: nil)
      |> Transformers.drop_nil_values()
      |> Transformers.rename_key(:number, :float)
      |> Transformers.parse_float(:float)
      |> Transformers.parse_datetime(:created, "{YYYY}-{0M}-{D}")

    assert output == %{
             output_key: false,
             unused: :unused,
             float: 10.0,
             created: ~N[2016-02-29 00:00:00]
           }

    assert audit |> Enum.map(& &1.name) == [
             :parse_datetime,
             :parse_float,
             :rename_key,
             :drop_nil_values,
             :canonicalize,
             :canonicalize,
             :rename_key
           ]
  end

  describe "map" do
    test "when value is a list" do
      input = %{a: [%{b: "1"}, %{b: "2"}]}
      expected = %{a: [%{c: 1}, %{c: 2}]}

      map_function = fn item ->
        item
        |> Transformers.rename_key(:b, :c)
        |> Transformers.parse_int(:c)
      end

      {:ok, output, audit} = input |> Transformers.map(:a, map_function)

      assert output == expected

      assert audit == [
               %AuditEntry{
                 name: :map,
                 args: [:a, map_function],
                 input: input,
                 output: expected,
                 sub_entries: [
                   [
                     %Changer.Transformer.AuditEntry{
                       args: [:c],
                       input: %{c: "2"},
                       name: :parse_int,
                       output: %{c: 2},
                       sub_entries: []
                     },
                     %Changer.Transformer.AuditEntry{
                       args: [:b, :c],
                       input: %{b: "2"},
                       name: :rename_key,
                       output: %{c: "2"},
                       sub_entries: []
                     }
                   ],
                   [
                     %Changer.Transformer.AuditEntry{
                       args: [:c],
                       input: %{c: "1"},
                       name: :parse_int,
                       output: %{c: 1},
                       sub_entries: []
                     },
                     %Changer.Transformer.AuditEntry{
                       args: [:b, :c],
                       input: %{b: "1"},
                       name: :rename_key,
                       output: %{c: "1"},
                       sub_entries: []
                     }
                   ]
                 ]
               }
             ]
    end

    test "when value is an empty list" do
      input = %{a: []}
      map_function = &Transformers.parse_int(&1, :b)

      {:ok, output, audit} = input |> Transformers.map(:a, map_function)
      assert output == input

      assert audit == [
               %Changer.Transformer.AuditEntry{
                 args: [:a, map_function],
                 input: %{a: []},
                 name: :map,
                 output: %{a: []},
                 sub_entries: []
               }
             ]
    end

    test "when a child transformer generates an error" do
      input = %{a: [%{b: "twelve"}, %{b: "2"}]}
      map_function = &Transformers.parse_int(&1, :b)

      {:error, error} = input |> Transformers.map(:a, map_function)

      assert error == %Error{
               args: [:a, map_function],
               input: input,
               reason: %Changer.Transformer.Error{
                 args: [:b],
                 input: %{b: "twelve"},
                 reason: "could not parse value to a int",
                 operation: :parse_int
               },
               operation: :map
             }
    end

    test "when the value is not a list" do
      input = %{a: "1"}
      map_function = &Transformers.parse_int(&1, :b)

      {:error, error} = input |> Transformers.map(:a, map_function)

      assert error == %Error{
               args: [:a, map_function],
               input: input,
               reason: "value was not a list",
               operation: :map
             }
    end

    test "when the list contains a non-map value" do
      input = %{a: ["1"]}
      map_function = &Transformers.parse_int(&1, :b)

      {:error, error} = input |> Transformers.map(:a, map_function)

      assert error == %Error{
               args: [:a, map_function],
               input: input,
               reason: "list member was not a map",
               operation: :map
             }
    end
  end

  describe "parse_float" do
    test "parsing a valid float" do
      {:ok, output, audit} = %{float_key: "-10.0"} |> Transformers.parse_float(:float_key)
      assert output == %{float_key: -10.0}

      assert audit == [
               %AuditEntry{
                 name: :parse_float,
                 args: [:float_key],
                 input: %{float_key: "-10.0"},
                 output: %{float_key: -10.0}
               }
             ]
    end

    test "parsing a valid whole number into a float" do
      {:ok, output, audit} = %{int_key: "10"} |> Transformers.parse_float(:int_key)
      assert output == %{int_key: 10.0}

      assert audit == [
               %AuditEntry{
                 name: :parse_float,
                 args: [:int_key],
                 input: %{int_key: "10"},
                 output: %{int_key: 10.0}
               }
             ]
    end

    test "parsing an string that's an invalid float" do
      input = %{float_key: "pi"}

      {:error, error} = Transformers.parse_float(input, :float_key)

      assert error == %Error{
               reason: "could not parse value to a float",
               input: input,
               args: [:float_key],
               operation: :parse_float
             }
    end

    test "parsing something that's already the target value" do
      input = %{float_key: 100.1}

      {:error, error} = Transformers.parse_float(input, :float_key)

      assert error == %Error{
               reason: "value was not a string",
               input: input,
               args: [:float_key],
               operation: :parse_float
             }
    end

    test "parsing something that makes no sense" do
      input = %{float_key: :hi}

      {:error, error} = Transformers.parse_float(input, :float_key)

      assert error == %Error{
               reason: "value was not a string",
               input: input,
               args: [:float_key],
               operation: :parse_float
             }
    end

    test "when the key does not exist in the input data" do
      input = %{unused: true}
      {:ok, output, audit} = Transformers.parse_float(input, :anything)
      assert output == input

      assert audit == [
               %AuditEntry{
                 name: :parse_float,
                 args: [:anything],
                 input: input,
                 output: input
               }
             ]
    end
  end

  describe "parse_int" do
    test "parsing a valid int" do
      {:ok, output, audit} = %{int_key: "-10"} |> Transformers.parse_int(:int_key)
      assert output == %{int_key: -10}

      assert audit == [
               %AuditEntry{
                 name: :parse_int,
                 args: [:int_key],
                 input: %{int_key: "-10"},
                 output: %{int_key: -10}
               }
             ]
    end

    test "parsing a float into a int" do
      input = %{int_key: "10.8"}
      {:error, error} = input |> Transformers.parse_int(:int_key)

      assert error == %Error{
               reason: "could not parse value to a int",
               input: input,
               args: [:int_key],
               operation: :parse_int
             }
    end

    test "parsing an string that's an invalid int" do
      input = %{int_key: "pi"}

      {:error, error} = Transformers.parse_int(input, :int_key)

      assert error == %Error{
               reason: "could not parse value to a int",
               input: input,
               args: [:int_key],
               operation: :parse_int
             }
    end

    test "parsing something that's already the target value" do
      input = %{int_key: 100.1}

      {:error, error} = Transformers.parse_int(input, :int_key)

      assert error == %Error{
               reason: "value was not a string",
               input: input,
               args: [:int_key],
               operation: :parse_int
             }
    end

    test "parsing something that makes no sense" do
      input = %{int_key: :hi}

      {:error, error} = Transformers.parse_int(input, :int_key)

      assert error == %Error{
               reason: "value was not a string",
               input: input,
               args: [:int_key],
               operation: :parse_int
             }
    end

    test "when the key does not exist in the input data" do
      input = %{unused: true}
      {:ok, output, audit} = Transformers.parse_int(input, :anything)
      assert output == input

      assert audit == [
               %AuditEntry{
                 name: :parse_int,
                 args: [:anything],
                 input: input,
                 output: input
               }
             ]
    end
  end

  describe "string to date" do
    test "parsing a valid string" do
      {:ok, output, audit} = %{created: "2016-02-29"} |> Transformers.parse_datetime(:created, "{YYYY}-{0M}-{D}")

      assert output == %{created: ~N[2016-02-29 00:00:00]}

      assert audit == [
               %AuditEntry{
                 name: :parse_datetime,
                 args: [:created, "{YYYY}-{0M}-{D}"],
                 input: %{created: "2016-02-29"},
                 output: output
               }
             ]
    end

    test "parsing an invalid string" do
      input = %{created: "not a datetime"}

      {:error, error} = Transformers.parse_datetime(input, :created, "{YYYY}-{0M}-{D}")

      assert error ==
               %Error{
                 args: [:created, "{YYYY}-{0M}-{D}"],
                 input: %{created: "not a datetime"},
                 reason: "unable to parse datetime",
                 operation: :parse_datetime
               }
    end

    test "parsing something that isn't a string" do
      input = %{created: 10}

      {:error, error} = Transformers.parse_datetime(input, :created, "{YYYY}-{0M}-{D}")

      assert error ==
               %Error{
                 args: [:created, "{YYYY}-{0M}-{D}"],
                 input: %{created: 10},
                 reason: "value was not a string",
                 operation: :parse_datetime
               }
    end

    test "when the key does not exist in the input data" do
      input = %{destroyed: "2016-02-29"}
      {:ok, output, audit} = Transformers.parse_datetime(input, :created, "{YYYY}-{0M}-{D}")
      assert output == %{destroyed: "2016-02-29"}

      assert audit == [
               %AuditEntry{
                 name: :parse_datetime,
                 args: [:created, "{YYYY}-{0M}-{D}"],
                 input: input,
                 output: input
               }
             ]
    end
  end

  describe "parse_set" do
    test "parsing a whitespace-delimited set" do
      input = %{key: "a b a     c \t d"}
      {:ok, output, audit} = Transformers.parse_set(input, :key)
      assert output == %{key: MapSet.new(["a", "b", "c", "d"])}

      assert audit == [
               %AuditEntry{
                 name: :parse_set,
                 args: [:key],
                 input: %{key: "a b a     c \t d"},
                 output: %{key: MapSet.new(["a", "b", "c", "d"])}
               }
             ]
    end

    test "parsing something that is not a string" do
      input = %{key: 10}

      {:error, error} = Transformers.parse_set(input, :key)

      assert error ==
               %Error{
                 args: [:key],
                 input: input,
                 reason: "value was not a string",
                 operation: :parse_set
               }
    end

    test "when the key does not exist in the input data" do
      input = %{wrong_key: "hello world"}
      {:ok, output, audit} = Transformers.parse_set(input, :right_key)
      assert output == %{wrong_key: "hello world"}

      assert audit == [
               %AuditEntry{
                 name: :parse_set,
                 args: [:right_key],
                 input: input,
                 output: input
               }
             ]
    end
  end

  describe "values_as_list" do
    test "when value of key is a map" do
      input_data = %{input: %{a: 1, b: 2, c: 3}, unused: :value}
      expected = %{input: [1, 2, 3], unused: :value}
      {:ok, output, audit_trail} = Transformers.values_as_list(input_data, :input)

      assert audit_trail == [
               %AuditEntry{
                 name: :values_as_list,
                 args: [:input],
                 input: input_data,
                 output: expected
               }
             ]

      assert output == expected
    end

    test "when key does not exist" do
      input_data = %{unused: :value}
      {:error, error} = Transformers.values_as_list(input_data, :input)

      assert error == %Error{
               args: [:input],
               input: input_data,
               reason: "value is not a map",
               operation: :values_as_list
             }
    end

    test "when value of key is not a map" do
      input_data = %{input: "stuff", unused: :value}
      {:error, error} = Transformers.values_as_list(input_data, :input)

      assert error == %Error{
               args: [:input],
               input: input_data,
               reason: "value is not a map",
               operation: :values_as_list
             }
    end
  end

  test "camelizing keys" do
    input = %{:a => 1, :snake_case => 2, :alreadyCamel => 3, "PascalCase" => 4}
    {:ok, output, audit} = Transformers.camelize_keys(input)
    assert output == %{"a" => 1, "alreadyCamel" => 3, "snakeCase" => 2, "pascalCase" => 4}

    assert audit == [
             %AuditEntry{
               name: :camelize_keys,
               args: [],
               input: input,
               output: output
             }
           ]
  end

  test "underscoring keys" do
    input = %{:a => 1, :snake_case => 2, :camelCase => 3, "PascalCase" => 4}
    {:ok, output, audit} = Transformers.underscore_keys(input)
    assert output == %{"a" => 1, "snake_case" => 2, "camel_case" => 3, "pascal_case" => 4}

    assert audit == [
             %AuditEntry{
               name: :underscore_keys,
               args: [],
               input: input,
               output: output
             }
           ]
  end

  test "drop nil values" do
    input = %{a: 1, b: 2, c: nil}
    {:ok, output, audit} = Transformers.drop_nil_values(input)
    assert output == %{a: 1, b: 2}

    assert audit == [
             %AuditEntry{
               name: :drop_nil_values,
               args: [],
               input: input,
               output: output
             }
           ]
  end
end
