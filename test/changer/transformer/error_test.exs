defmodule Changer.Transformer.ErrorTest do
  use ExUnit.Case

  alias Changer.Transformer.Error

  describe "message" do
    test "when reason is a String" do
      assert %Error{reason: "it was bad"} |> Exception.message() == "it was bad"
    end

    test "when reason is another Error" do
      assert %Error{reason: %Error{reason: %Error{reason: "it was really bad"}}} |> Exception.message() ==
               "it was really bad"
    end
  end
end
