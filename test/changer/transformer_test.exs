defmodule Changer.TransformerTest do
  use ExUnit.Case

  alias Changer.Transformer

  describe "value" do
    test "removes audit trail from tuple" do
      assert Transformer.value({:ok, %{a: 1}, []}) == {:ok, %{a: 1}}
    end

    test "passes error through as-is" do
      assert Transformer.value({:error, %Transformer.Error{}}) == {:error, %Transformer.Error{}}
    end
  end

  describe "value!" do
    test "returns value" do
      assert Transformer.value!({:ok, %{a: 1}, []}) == %{a: 1}
    end

    test "raises on error" do
      assert_raise Transformer.Error, fn ->
        Transformer.value!({:error, %Transformer.Error{}})
      end
    end
  end
end
