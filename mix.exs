defmodule Changer.MixProject do
  use Mix.Project

  def project do
    [
      aliases: aliases(),
      app: :changer,
      deps: deps(),
      description: description(),
      dialyzer: dialyzer(),
      elixir: "~> 1.10",
      package: package(),
      preferred_cli_env: [t: :test],
      start_permanent: Mix.env() == :prod,
      version: "0.1.0"
    ]
  end

  defp aliases do
    [
      t: [
        "compile --force --warnings-as-errors",
        "test --cover",
        "credo",
        "dialyzer"
      ]
    ]
  end

  defp dialyzer do
    [
      plt_add_apps: [:inflex],
      plt_add_deps: :app_tree,
      plt_file: {:no_warn, "priv/plts/dialyzer.plt"}
    ]
  end

  defp description do
    """
    Changer provides tools for transforming data, resolving conflicts, and accumulating audit visibility into those behaviors
    """
  end

  defp package do
    [
      files: ["lib", "priv", "mix.exs", "README.md", "LICENSE.md"],
      licenses: ["MIT"],
      links: %{
        GitLab: "https://gitlab.com/geometerio/changer"
      },
      maintainers: ["geometer, llc"]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      applications: [:timex]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:credo, "~> 1.4", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.0", only: [:dev, :test], runtime: false},
      {:ex_doc, "~> 0.22", only: [:dev, :test], runtime: false},
      {:inflex, "~> 2.0.0"},
      {:magritte, "~> 0.1.2"},
      {:mix_audit, "~> 0.1", only: [:dev, :test], runtime: false},
      {:timex, "~> 3.0"}
    ]
  end
end
